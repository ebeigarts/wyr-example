var introSlide = document.querySelector('.intro-slide');
var prefsSlide = document.querySelector('.pref-slide');
var prefs = document.querySelectorAll('.pref');

var names = [
  'Justin Timberlake',
  'Justin Bieber',
  'Ryan Reynolds',
  'Zac Efron',
  'Jake Gyllenhaal',
  'Ashton Kutcher',
  'Patric J Adams',
  'Jared Leto',
  'Simon Helberg',
  'Penn Badgley',
  'Jason Segel',
  'John Krasinski',
  'James Franco',
  'Aaron Paul',
  'Christian Bale',
  'Nathan Fillion',
  'Chris Noth',
  'Patrick Dempsey',
  'Adam Driver',
  'Peter Dinklage',
  'Jimmy Fallon',
  'Rami Malek',
  'Jesse Williams',
  'Jake Johnson',
  'Jeremy Piven',
  'Jude Law',
  'Ryan Gosling',
  'Justin Chambers',
  'Bradley Cooper',
  'Leonardo DiCaprio',
  'Brian Austin Green',
  'Matt Czuchry',
  'Robert Downey Jr',
  'Matthew Perry',
  'Ģirts Ķesteris',
  'David Boreanaz',
  'Liam Hemsworth',
  'Channing Tatum',
  'Chris Evans'
];

var imageURL = function(name) {
  return 'images/' + encodeURIComponent(name) + '.jpg';
}

// Preload all images
names.forEach(function(name) {
  var image = new Image();
  console.log('Preloading', name);
  image.src = imageURL(name);
});

var selectPref = function(i) {
  var j = 1 - i;

  prefs[i].classList.add('active');
  setTimeout(function() {
    prefs[i].classList.remove('active');
    changePref(prefs[j]);
  }, 400);
}

var changePref = function(pref) {
  var name = names.shift();
  if (!name) {
    name = 'Ģirts Lācis';
  }
  pref.classList.add('change');
  setTimeout(function() {
    pref.querySelector('h2').innerHTML = name;
    var img = pref.querySelector('img');
    img.onload = function() {
      pref.classList.remove('change');
    }
    img.src = imageURL(name);
  }, 400);
};

introSlide.addEventListener('click', function() {
  introSlide.style.display = 'none'
  prefsSlide.style.display = '';
  changePref(prefs[0]);
  changePref(prefs[1]);
});

Array.prototype.forEach.call(prefs, function(el) {
  el.addEventListener('click', function() {
    console.log('Select');
    if (el == prefs[0]) {
      console.log('Select 1');
      selectPref(0);
    } else {
      console.log('Select 2');
      selectPref(1);
    }
  });
});
